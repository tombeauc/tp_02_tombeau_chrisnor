package com.gmail.tombeauc.tp02_tombeau_chrisnor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.BreakIterator;

public class ComputeActivity extends AppCompatActivity {
    public static final int DIVIDE_BY_ZERO = 0;
    private  TextView textViewNb1;
    private TextView textViewNb2;
    private Button buttonSum;
    private Button buttonMinus;
    private Button buttonMultiply;
    private Button buttonDivide;

    private void setUpViews() {

        textViewNb1 = findViewById(R.id.textNb1);
        textViewNb2 = findViewById(R.id.textNb2);
        buttonSum = findViewById(R.id.sum);
        buttonMinus = findViewById(R.id.minus);
        buttonMultiply = findViewById(R.id.multiply);
        buttonDivide = findViewById(R.id.divide);

        Intent intent = getIntent();
        String nb1 = intent.getStringExtra("nb1");
        String nb2 = intent.getStringExtra("nb2");

        textViewNb1.setText("Nombre 1 : "+ nb1);
        textViewNb2.setText("Nombre 2 : "+ nb2);

        buttonSum.setOnClickListener(new View.OnClickListener() {
            // @Override
            public void onClick(View view) {
                int result = Integer.parseInt(nb1) + Integer.parseInt(nb2);
                Intent intent = new Intent();
                intent.putExtra("result", result);
                setResult(RESULT_OK, intent);
                Toast.makeText(ComputeActivity.this, " somme: " + result, Toast.LENGTH_LONG).show();
                finish();
            }
        });

        buttonMinus.setOnClickListener(new View.OnClickListener() {
            // @Override
            public void onClick(View view) {
                int minus = Integer.parseInt(nb1) - Integer.parseInt(nb2);
                Intent intent = new Intent();
                Intent result = intent.putExtra("result", minus);
                setResult(RESULT_OK, intent);
                Toast.makeText(ComputeActivity.this, " Soustraction: " + minus, Toast.LENGTH_LONG).show();
                finish();
            }
        });
        buttonMultiply.setOnClickListener(new View.OnClickListener() {
            // @Override
        public void onClick(View view) {
            int multiply = Integer.parseInt(nb1) * Integer.parseInt(nb2);
            Intent intent = new Intent();
            Intent result = intent.putExtra("result", multiply);
            setResult(RESULT_OK, intent);
            Toast.makeText(ComputeActivity.this, " Multiplication: " + multiply, Toast.LENGTH_LONG).show();
            finish();
        }
    });

        buttonDivide.setOnClickListener(new View.OnClickListener() {
            // @Override
         public void onClick(View view) {
        if(Integer.parseInt(nb2)!=0){
        Float divide = Float.parseFloat(nb1) / Float.parseFloat(nb2);
        Intent intent = new Intent();
        Intent result = intent.putExtra("result", divide);
        setResult(RESULT_OK, intent);
        Toast.makeText(ComputeActivity.this, " Division: " + divide, Toast.LENGTH_LONG).show();
        finish();
    }
        else {
          setResult(DIVIDE_BY_ZERO, intent);
       }
    }
});
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compute);
        setUpViews();
    }
}
