package com.gmail.tombeauc.tp02_tombeau_chrisnor;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final int COMPUTE_CODE = 123;
    private EditText editTextNb1;
    private EditText editTextNb2;
    private Button buttonCompute;
    private Intent data;

    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextNb1 = findViewById(R.id.nb1);
        editTextNb2 = findViewById(R.id.nb2);
        buttonCompute = findViewById(R.id.compute);
        //buttonCompute.setEnabled(false);
        initViews();
    }

    private void initViews() {
        editTextNb1 = findViewById(R.id.nb1);
        editTextNb2 = findViewById(R.id.nb2);
        buttonCompute = findViewById(R.id.compute);

        //Intercept click on the compute button
        buttonCompute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Get the contents of the input texts
                String textNb1 = editTextNb1.getText().toString();
                String textNb2 = editTextNb2.getText().toString();
                Intent intent = new Intent(MainActivity.this, ComputeActivity.class);
                intent.putExtra("nb1", textNb1);
                intent.putExtra("nb2", textNb2);
                startActivity(intent);
                Toast.makeText(MainActivity.this, "Calcul avec les nombres: " + textNb1 + " et " + textNb2, Toast.LENGTH_LONG).show();
            }
        });

    }



    }
